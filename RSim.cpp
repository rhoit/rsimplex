//# define TestMode

# include <iostream>
using namespace std;

# include <cstring>
# include <ncurses.h>
# include <cmath>

# define loop_limit 5

# define dtab 8
# define fix 3
int x, y;
char FRAC=0;

typedef struct Table *Tptr;
struct Table {
  unsigned int m, n;
  unsigned int nbv, bv, av;
  
  char* aTag; //attrib row tag
  char* bTag; //basic var col tag
  
  float **dx; //table data
  
  Table();
  Table(Tptr);
  Table(int, int, int);
  
  ~Table();
  
  char* pvcod(char); //print varcode
  int avcod(char*, char*&); //assign varcode

  void dbug();
  void show();
};

class Simplex {
private:
  int sample_flag;
  unsigned int pivot_i, pivot_j;
  
  float pivot;
  float Temp1, Temp2;
  
public:
  Tptr tb;
  Tptr TTmp;
  
  Simplex();
  
  int show(Tptr);
  
  void next_table(Tptr, Tptr);
  int outvar(Tptr);
  void process_pivot(Tptr, Tptr);
  void insert();
  
  Tptr basicMax(Tptr);
  Tptr basicMin(Tptr);
  Tptr dualMin(Tptr);
  Tptr integer(Tptr);
  
  void menu(char);
  
  char Samples(char);
  Tptr Sample_sim_max();
  Tptr Sample_sim_min();
  Tptr Sample_dual_min();
  Tptr Sample_int_max();
};

int dtemp=1;
int ret;
int Dec2Fra(float);
float de;
char pfrac[6];
void pno(float);
char mask[dtab];

int main() {
  initscr();
  getch();

  Simplex s;
  s.Samples('9');
  s.menu('1');
  
  refresh();
  endwin();
  return 0;
}

int Dec2Fra(float dec_part) {
  int int_part=(int)dec_part;
  dec_part-=int_part;
  
  if(dec_part<0.005) return int_part;
  
  ret=Dec2Fra(1/dec_part);
  
  int_part*=ret;
  int_part+=dtemp;

  dtemp=ret;
  return int_part;
}

void pno(float no) {
  if(FRAC==1) goto kick;
  de=no-(int)no;
  if(de!=0 && fabs(de)>.0005) {
    dtemp=1;
    if(no<0) sprintf(&mask[0], "-%d/", Dec2Fra(-no));
    else sprintf(&mask[0], "%d/", Dec2Fra(no));
    sprintf(&pfrac[0], "%s%d",mask, dtemp);
    printw("%*s", dtab, pfrac);
	}
  else
  kick: printw("%*.*f", dtab, fix, no);
}

Table::Table() {
  nbv=0; bv=0; av=0;
  m=0; n=0;
  
  aTag=NULL;
  bTag=NULL;
  
  dx=NULL;
  
  printw("Table::Table() default constructor\n");
}

Table::Table(Tptr scr) {
  nbv=scr->nbv; bv=scr->bv; av=scr->av;
  m=scr->m; n=scr->n;
  
  aTag = new char[n];
  for(int i=1; i<n; i++) aTag[i]=scr->aTag[i];
  
  bTag = new char[m];
  for(int i=0; i<n; i++) bTag[i]=scr->bTag[i];
}

Table::Table(int nb, int b, int a) {
  nbv=nb; bv=b; av=a;
  m=b+1; n=nb+b+1;
}

Table::~Table() {
  delete [] aTag; delete [] bTag;
  
  for(int i=0; i<n; i++)
    delete []dx[i];
  delete [] dx;
}

char* Table::pvcod(char c) {
  if(c==32 || c=='r' || c=='z') goto skp;
  
  if(c>='a') sprintf(&mask[0],"x%d",c-'`');
  else if(c>='1') sprintf(&mask[0],"a%c",c);
  else if(c<=9) sprintf(&mask[0],"Tb #%d",c);
  else
  skp:sprintf(&mask[0]," %c", c);
  
  return &mask[0];
}

int Table::avcod(char* str, char *&Tag) {
  int len=strlen(str);
  Tag = new char[len/2+1];
  
  int x=1;
  for(int i=0; i<len; i++) 	  {
    if(*(str+i)>'0' && *(str+i)<='9') {
      if(*(str+i-1)=='x') *(Tag+x)=*(str+i)+48; //-48+96=48
      if(*(str+i-1)=='a') *(Tag+x)=*(str+i);
      x++;
    }
  }
  return len/2;
}

void Table::dbug() {
  printw("Table: %p",this);
  printw("\n\tnbv: %d bv: %d av: %d", nbv, bv, av);
  printw("\tm: %d n: %2d", m, n);
  printw("\n\taTag: %p\tbTag: %p", (void*)aTag, (void*)bTag);
  printw("\n\tdx: %p", dx);
}

void Table::show() {
#ifdef TestMode
  dbug();
  printw("\n");
#endif
  
  if(n==0) {
    cout<<"Can't Print Table: No variable assigned\n";
    return;
  }
  
  attron(A_BOLD | A_STANDOUT );
  printw("%*s ", -dtab, pvcod(aTag[0]));
  
  for(int i=1; i< n; i++)
    printw("%*s", -dtab, pvcod(aTag[i]));
  
  printw("RHS");
  
  printw("\n%s ", pvcod(bTag[0]));
  attroff(A_BOLD| A_STANDOUT );
  
  attron(A_UNDERLINE);
  for(int j=0; j<n-1; j++) pno(dx[0][j]);
  addch(ACS_VLINE);
  pno(dx[0][n-1]);
  attroff(A_UNDERLINE);
  
  for(int i=1; i<m; i++) {
    attron(A_BOLD| A_STANDOUT );
    printw("\n%s ", pvcod(bTag[i]));
    attroff(A_BOLD| A_STANDOUT );
    for(int j=0; j<n-1; j++) pno(dx[i][j]);
    addch(ACS_VLINE);
    pno(dx[i][n-1]);
    attroff(A_UNDERLINE);
  }
}

Simplex::Simplex() {
  sample_flag=0;
  tb=NULL;
  TTmp=NULL;
}

int Simplex::show(Tptr T) {
  if(T==NULL) {
    printw("\nError!! No Table Found... Null Pointer\n");
    return 1;
  }
  
  T->show();
  
  getyx(stdscr, y, x);
}

int Simplex::outvar(Tptr T1) {
  //Finding Leaving variable
  int i=1;
 reload:
  if(T1->dx[i][pivot_j]!=0) {
    Temp1=T1->dx[i][T1->n-1] / T1->dx[i][pivot_j];
    if(Temp1>0) goto ok;
  }
  i++;
  if(i>=T1->n) return NULL;//goto returning; //ending the loop
  goto reload;
  
 ok: pivot_i=i;
  
  for(i++; i<T1->m; i++) {
    if(T1->dx[i][T1->n-1]<=0 || T1->dx[i][pivot_j]<0) continue;
    Temp2=T1->dx[i][T1->n-1] / T1->dx[i][pivot_j];
    
    if(Temp1> Temp2) {
      Temp1=Temp2;
      pivot_i=i;
    }
  }
  return 1;
}

Tptr Simplex::integer(Tptr T1) {
  Tptr T2;
 re:
  Temp2=0;
  for(int i=1; i<=T1->bv; i++) {
    Temp1=T1->dx[i][T1->n-1];
    Temp1-=(int)Temp1;
    if(Temp1>Temp2) {
      Temp2=Temp1;
      pivot_i=i;
    }
  }
  
  if(Temp2==0.0 || Temp2 < .0005) {
    printw("\n>Solution is in approximate integer value");
    return T1;
  }
  
  //allocating memory
  T2=new Table(T1->nbv,T1->bv+1,0);
  
  T2->dx=new float* [T2->m];
  for(int i=0; i<T2->m; i++)
    T2->dx[i]=new float [T2->n];
  
  //Inserting data
  for(int i=0; i<T1->m; i++)
    for(int j=0; j<T1->n-1; j++)
      T2->dx[i][j]=T1->dx[i][j];
  
  //inserting RHS
  for(int i=0; i<T1->m; i++) T2->dx[i][T1->n]=T1->dx[i][T1->n-1];
  
  //Fixing Tags
  T2->aTag = new char[T2->n];
  for(int i=1; i<T1->n; i++) T2->aTag[i]=T1->aTag[i];
  T2->aTag[T1->n]='a'+T1->n-T1->av-1;;
  
  T2->bTag = new char[T2->m];
  for(int i=0; i<T1->m; i++) T2->bTag[i]=T1->bTag[i];
  T2->bTag[T1->m]=T2->aTag[T1->n];
  
  //Adding new variable
  Temp2=0;
  for(int i=1; i<T1->m; i++) {
    Temp1=T2->dx[i][T1->n];
    Temp1-=(int)Temp1;
    if(Temp1>Temp2) {
      Temp2=Temp1;
      pivot_i=i;
    }
  }
  
  T2->dx[T1->m][T1->n]=-Temp2;
  
  //pivot_i++;
  for(int j=0; j<T1->n; j++) {
    Temp1=T1->dx[pivot_i][j];
    Temp1-=(int)Temp1;
    T2->dx[T1->m][j]=-Temp1;
  }
  
  T2->dx[T1->m][T1->n-1]=1;
  
  Simplex::show(T2);
  getch();
  clear();
  
  refresh();
  dualMin(T2);
  return 0;
}

Tptr Simplex::basicMax(Tptr T1) {
  //Finding Entering Variable
re: Temp1=T1->dx[0][0];
  pivot_j=0;
  for(int j=1; j<T1->nbv; j++) {
    if(Temp1 > T1->dx[0][j]) {
      Temp1= T1->dx[0][j];
      pivot_j=j;
    }
  }

  if(Temp1>=0) goto returning; //get -ve to cont..

  //Finding Leaving variable
  if(!Simplex::outvar(T1)) goto returning;
  
  Tptr T2;
  if(T1->aTag[0]==0) { //Allocate Memory for first use
    T2=new Table(T1);
    
    T2->dx=new float* [T2->m];
    for(int i=0; i<T2->m; i++)
      T2->dx[i]=new float [T2->n];
  }
  
  Simplex::next_table(T1,T2);
  
  if(T2->aTag[0]>loop_limit) {
    printw("Get OFF !! off limit");
    return 0;
  }
  
  if(T2->aTag[0]==1) { //1st run only
    T1=T2;
    T2=new Table(T1);
    
    T2->dx=new float* [T2->m];
    for(int i=0; i<T2->m; i++)
      T2->dx[i]=new float [T2->n];
    goto re;
  }
  
  TTmp=T2; T2=T1; T1=TTmp;
  goto re;
  
 returning:
  if(T2!=NULL) if(T2->aTag[0]!=1) delete T2; //avoid deleting original table
  return T1;
}

Tptr Simplex::dualMin(Tptr T1) {
 re:
  //Finding Leaving variable
  Temp1=T1->dx[1][T1->n-1];
  pivot_i=1;
  for(int i=1; i<T1->m; i++) {
    if(Temp1 > T1->dx[i][T1->n-1]) {
      Temp1= T1->dx[i][T1->n-1];
      pivot_i=i;
    }
  }
  
  if(Temp1>0) goto returning; //get -ve to cont..

  
  //Finding Entering Variable
  Temp2=T1->dx[0][0] / T1->dx[pivot_i][0];
  if(Temp2<0) Temp2*=(-1.0);
  pivot_j=0;
  for(int j=1; j<T1->nbv; j++) {
    Temp1=T1->dx[0][j] / T1->dx[pivot_i][j];
    if(Temp1<0) Temp1*=(-1.0);
    if(Temp2> Temp1) {
      Temp2=Temp1;
      pivot_j=j;
    }
  }
  
  Tptr T2;
  if(T1->aTag[0]==0) { //Allocate Memory for first use
    T2=new Table(T1);
    
    T2->dx=new float* [T2->m];
    for(int i=0; i<T2->m; i++)
      T2->dx[i]=new float [T2->n];
  }
  
  Simplex::next_table(T1,T2);
  
  if(T2->aTag[0]>loop_limit) {
    printw("Get OFF !! off limit");
    return 0;
  }
  
  if(T2->aTag[0]==1) {//1st run only
    T1=T2;
    T2=new Table(T1);
    
    T2->dx=new float* [T2->m];
    for(int i=0; i<T2->m; i++)
      T2->dx[i]=new float [T2->n];
    goto re;
  }
  
  TTmp=T2; T2=T1; T1=TTmp;
  goto re;
  
 returning:
  if(T2!=NULL) if(T2->aTag[0]!=1) delete T2; //avoid deleting original table
  return T1;
}

Tptr Simplex::basicMin(Tptr T1) {
 re:
  if(T1->dx[0][T1->n] ==0) goto returning;
  //Finding Entering Variable
  Temp1=T1->dx[0][0];
  pivot_j=0;
  for(int j=1; j<T1->n-1; j++) {
    if(Temp1 < T1->dx[0][j]) {
      Temp1= T1->dx[0][j];
      pivot_j=j;
    }
  }
  if(Temp1<=0) goto returning; //get +ve to cont..
  
  //Finding Leaving variable
  if(!Simplex::outvar(T1)) goto returning;
  
  Tptr T2;
  if(T1->aTag[0]==0) { //Allocate Memory for first use
    T2=new Table(T1);
    
    T2->dx=new float* [T2->m];
    for(int i=0; i<T2->m; i++)
      T2->dx[i]=new float [T2->n];
  }
  
  Simplex::next_table(T1,T2);
  
  if(T2->aTag[0]>loop_limit) {
    printw("Get OFF !! off limit");
    return 0;
  }
  
  if(T2->aTag[0]==1) { //1st run only
    T1=T2;
    T2=new Table(T1);
    
    T2->dx=new float* [T2->m];
    for(int i=0; i<T2->m; i++)
      T2->dx[i]=new float [T2->n];
    goto re;
  }
  
  TTmp=T2; T2=T1; T1=TTmp;
  goto re;
  
 returning:
  if(T2!=NULL) if(T2->aTag[0]!=1) delete T2; //avoid deleting original table
  return T1;
}

void Simplex::next_table(Tptr T1, Tptr T2) {
  process_pivot(T1, T2);
  for(int i=0; i<T1->m; i++) {
    for(int j=0; j<T1->n; j++) {
      if(pivot_i==i) T2->dx[i][j]=T1->dx[i][j]/pivot;
      else T2->dx[i][j]=T1->dx[i][j]-T1->dx[pivot_i][j]*T1->dx[i][pivot_j]/pivot;
    }
  }
  printw("\n");
  Simplex::show(T2);
}

void Simplex::process_pivot(Tptr T1, Tptr T2) {
  pivot=T1->dx[pivot_i][pivot_j];
  
  mvchgat(y-(T1->bv)+pivot_i, (pivot_j*dtab)+3, dtab, A_BOLD, 1, NULL);
  mvprintw(0, 65, "pivot_ij: (%d,%d)", pivot_i, pivot_j);
  move(y,x);

  T2->aTag[0]=T1->aTag[0]+1;
  T2->bTag[pivot_i]=T1->aTag[pivot_i];
}

void Simplex::menu(char ch) {
  Tptr Solution=NULL;
  Tptr Zombie;
  if(ch!=0) goto ch_chk;
  
  cbreak();

 Menu_loop:
  attron(A_BOLD | A_UNDERLINE);
  printw("Revised Simplex Method\n");
  attroff(A_BOLD | A_UNDERLINE );
  printw("\n0. Fill the table\n");
  printw("\n1. Max Optimizition");
  printw("\n2. Min Optimizition");
  printw("\n3. Min Dual Method");
  printw("\n\n(9) Integer Programming");
  printw("\n(+) Optimal Table");
  printw("\n\nEnter your choice: ");
  refresh();

 def:
  ch=getch();
  
 ch_chk:
  switch(ch) {
  case '0':
    clear();
    Simplex::insert();
    goto Menu_loop;
    
  case '1':
    clear();
    printw(">Maximize:\n");
    Simplex::show(tb);
    if(Solution!=NULL)
      if(Solution->aTag[0]==0)
	delete Solution;
    Solution=Simplex::basicMax(tb);
    Zombie=Solution;
    goto extended_view_sol;
    
  case '2':
    clear();
    printw(">Minimize:\n");
    Simplex::show(tb);
    if(Solution!=NULL)
      if(Solution->aTag[0]==0)
	delete Solution;
    Solution=Simplex::basicMin(tb);
    Zombie=Solution;
    goto extended_view_sol;
    
  case '3':
    clear();
    printw(">Dual Method Minimize:\n");
    Simplex::show(tb);
    if(Solution!=NULL)
      if(Solution->aTag[0]==0)
	delete Solution;
    Solution=Simplex::dualMin(tb);
    Zombie=Solution;
    goto extended_view_sol;
    
  case '9':
    clear();
    printw(">Integer Program:\n");
    if(Simplex::show(Solution)) {
      getch();
      break;
    }
    Zombie=Simplex::integer(Solution);
    goto extended_view_sol;
    
  case '+':
    printw("%c>Optimal Table:\n",13);
    Zombie=Solution;
    goto extended_view;
    
  case '`':
    ch=Samples(0);
    goto ch_chk;
    break;
    
  case 10:
    printw(">Orignal Table:\n");
    Zombie=tb;
    goto extended_view;
    
  case 27: return;
    
  case '.':
    FRAC=!FRAC;
    
    //			default: //for blinking
    //				goto Menu_loop;
    //			//goto def;
  }
  clear();
  goto Menu_loop;
  
 extended_view:
  Simplex::show(Zombie);
  refresh();
  
 extended_view_sol:
  switch(getch()) {
  case '.':
    FRAC=!FRAC;
    printw("\n");
    Simplex::show(Zombie);
    refresh();
    FRAC=!FRAC;
    goto extended_view_sol;
    
  case '9':
    printw("%c\n>Integer Program:\n",8);
    Zombie=Simplex::integer(Solution);
    goto extended_view_sol;
  }
  clear();
  goto Menu_loop;
}

Tptr Simplex::Sample_sim_min() {
  TTmp=new Table(3,3,0);
  
  TTmp->avcod("x1x2x3x4a1a2", TTmp->aTag);	TTmp->aTag[0]=0;
  TTmp->avcod("a1x3a2", TTmp->bTag); TTmp->bTag[0]='z';
  
  float sim_min[4][7] =
    {
      7,  4,  0, -1,  0,  0,  9,
      3,  1,  0,  0,  1,  0,  3,
      1,  2,  1,  0,  0,  0,  4,
      4,  3,  0, -1,  0,  1,  6
    };
  
  TTmp->dx=new float* [TTmp->m];
  for(int i=0; i<TTmp->m; i++)
    TTmp->dx[i]=new float [TTmp->n];
  
  for(int i=0; i<TTmp->m; i++)
    for(int j=0; j<TTmp->n; j++)
      TTmp->dx[i][j]=sim_min[i][j];
  
  return TTmp;
}

Tptr Simplex::Sample_dual_min() {
  TTmp=new Table(2,3,0);
  
  TTmp->avcod("x1x2x3x4x5", TTmp->aTag);	TTmp->aTag[0]=0;
  TTmp->avcod("x3x4x5", TTmp->bTag); TTmp->bTag[0]='z';
  
  float dual_min[4][6] =
    {
      -3, -2,  0,  0,  0,  0,
      -3, -1,  1,  0,  0, -3,
      -4, -3,  0,  1,  0, -6,
      1,  1,  0,  0,  1,  3
    };

  TTmp->dx=new float* [TTmp->m];
  for(int i=0; i<TTmp->m; i++)
    TTmp->dx[i]=new float [TTmp->n];
  
  for(int i=0; i<TTmp->m; i++)
    for(int j=0; j<TTmp->n; j++)
      TTmp->dx[i][j]=dual_min[i][j];
  
  return TTmp;
}

Tptr Simplex::Sample_int_max()
{
  TTmp=new Table(2,2,0);
  
  TTmp->avcod("x1x2x3x4", TTmp->aTag);	TTmp->aTag[0]=0;
  TTmp->avcod("x3x4", TTmp->bTag); TTmp->bTag[0]='z';
  
  float int_max[3][5] =
    {
      -7, -9,  0,  0,  0,
      -1,  3,  1,  0,  6,
      7,  1,  0,  1, 35,
    };
  
  TTmp->dx=new float* [TTmp->m];
  for(int i=0; i<TTmp->m; i++)
    TTmp->dx[i]=new float [TTmp->n];
  
  for(int i=0; i<TTmp->m; i++)
    for(int j=0; j<TTmp->n; j++)
      TTmp->dx[i][j]=int_max[i][j];
  
  return TTmp;
}


void Simplex::insert() {
  int bv, nbv, av;
  printw("\nbasic var: "); scanw("%d",&bv);
  printw("non basic var: "); scanw("%d",&nbv);
  printw("aritificial var: "); scanw("%d",&av);
  tb=new Table(bv,nbv,av);
  
  char srt[20];
  printw("aTag:  " ); scanw("%s",&nbv);
  tb->avcod(srt, tb->aTag);	tb->aTag[0]=0;
  
  printw("bTag:  " ); scanw("%s",&nbv);
  tb->avcod(srt, TTmp->bTag);  tb->bTag[0]='z';
  
  TTmp->dx=new float* [TTmp->m];
  for(int i=0; i<TTmp->m; i++)
    TTmp->dx[i]=new float [TTmp->n];
  
  show(tb);
  //	for(int i=0; i<TTmp->m; i++)
  //		for(int j=0; j<TTmp->n; j++)
  //			TTmp->dx[i][j]=int_max[i][j];
  //
}

Tptr Simplex::Sample_sim_max() {
  TTmp=new Table(2,4,0);
  
  TTmp->avcod("x1x2x3x4x5x6", TTmp->aTag); TTmp->aTag[0]=0;
  TTmp->avcod("x3x4x5x6", TTmp->bTag); TTmp->bTag[0]='z';
  
  float sim_max[5][7] =
    {
      -5,-4,  0,  0,  0,  0,  0,
      6,  4,  1,  0,  0,  0, 24,
      1,  2,  0,  1,  0,  0,  5,
      -1, 1,  0,  0,  1,  0,  1,
      0,  1,  0,  0,  0,  1,  2
    };
  
  TTmp->dx=new float* [TTmp->m];
  for(int i=0; i<TTmp->m; i++)
    TTmp->dx[i]=new float [TTmp->n];
  
  for(int i=0; i<TTmp->m; i++)
    for(int j=0; j<TTmp->n; j++)
      TTmp->dx[i][j]=sim_max[i][j];
  
  return TTmp;
}

enum method {
  halt=0,
  maximize='1',
  minimize='2',
  dualmini='3',
  integer='9'
};

char Simplex::Samples(char ch) {
  printw("\n>Sample:");
  if(ch==0) ch=getch();
  
  switch(ch) {
  case '1':
    if(tb!=NULL && sample_flag==0) delete tb;
    tb=Sample_sim_max();
    ch=maximize;
    break;
    
  case '2':
    if(tb!=NULL && sample_flag==0) delete tb;
    tb=Sample_sim_min();
    ch=minimize;
    break;
    
  case '3':
    if(tb!=NULL && sample_flag==0) delete tb;
    tb=Sample_dual_min();
    ch=dualmini;
    break;
    
  case '9':
    if(tb!=NULL && sample_flag==0) delete tb;
    tb=Sample_int_max();
    ch=maximize;
    break;
    
  default:
    printw("\nSample Not Found!!");
    getch();
    return halt;
  }
  
  sample_flag=1;
  return ch;
}
